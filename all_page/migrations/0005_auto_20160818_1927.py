# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0004_auto_20160817_2044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='description',
            field=models.TextField(max_length=40000, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='description_adicional',
            field=models.TextField(max_length=40000, null=True, blank=True),
        ),
    ]
