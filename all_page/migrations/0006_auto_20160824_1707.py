# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0005_auto_20160818_1927'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='description',
            field=tinymce.models.HTMLField(null=True, blank=True),
        ),
    ]
