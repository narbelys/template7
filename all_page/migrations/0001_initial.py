# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1500)),
            ],
        ),
        migrations.CreateModel(
            name='Most_Popular',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pedido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField(default=0)),
                ('price', models.FloatField(null=True, blank=True)),
                ('email', models.CharField(max_length=1500)),
                ('sent', models.IntegerField(default=0)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(upload_to=b'shoes_app/shoesstatic/shoes_app/photos', verbose_name=b'My Photo')),
                ('name', models.CharField(max_length=200)),
                ('description_short', models.CharField(max_length=1500)),
                ('description', models.CharField(max_length=3000)),
                ('description_adicional', models.CharField(max_length=3000)),
                ('price', models.FloatField(null=True, blank=True)),
                ('price_before', models.FloatField(null=True, blank=True)),
                ('status', models.IntegerField(default=1)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('photo1', models.ImageField(upload_to=b'static/images/home', null=True, verbose_name=b'My Photo1', blank=True)),
                ('photo2', models.ImageField(upload_to=b'static/images/home', null=True, verbose_name=b'My Photo2', blank=True)),
                ('photo3', models.ImageField(upload_to=b'static/images/home', null=True, verbose_name=b'My Photo3', blank=True)),
                ('photo4', models.ImageField(upload_to=b'static/images/home', null=True, verbose_name=b'My Photo4', blank=True)),
                ('categoria', models.ForeignKey(to='all_page.Category')),
            ],
        ),
        migrations.AddField(
            model_name='pedido',
            name='producto',
            field=models.ForeignKey(to='all_page.Producto'),
        ),
        migrations.AddField(
            model_name='most_popular',
            name='producto',
            field=models.ForeignKey(to='all_page.Producto'),
        ),
    ]
