# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0002_auto_20160817_1748'),
    ]

    operations = [
        migrations.CreateModel(
            name='Producto_home',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.ImageField(upload_to=b'static/images/home', verbose_name=b'My Photo')),
                ('producto', models.ForeignKey(to='all_page.Producto')),
            ],
        ),
    ]
