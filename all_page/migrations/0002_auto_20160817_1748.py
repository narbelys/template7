# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='photo',
            field=models.ImageField(upload_to=b'static/images/home', verbose_name=b'My Photo'),
        ),
    ]
