# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0003_producto_home'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='description',
            field=models.CharField(max_length=3000, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='description_adicional',
            field=models.CharField(max_length=3000, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='description_short',
            field=models.CharField(max_length=1500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='producto_home',
            name='price',
            field=models.ImageField(upload_to=b'static/images/home', null=True, verbose_name=b'My Photo', blank=True),
        ),
    ]
