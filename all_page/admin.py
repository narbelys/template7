from django.contrib import admin

from .models import Producto, Pedido, Most_Popular, Category, Producto_home



class ProductoAdmin(admin.ModelAdmin):
	list_display = ('name', 'description_short', 'admin_image')

class PedidoAdmin(admin.ModelAdmin):
        list_display = ('email', 'price', 'sent', 'producto')

class Most_PopularAdmin(admin.ModelAdmin):
        list_display = ('pk', 'producto')

class CategoryAdmin(admin.ModelAdmin):
        list_display = ('pk', 'name')
        
class Producto_homeAdmin(admin.ModelAdmin):
        list_display = ('pk', 'producto')



admin.site.register(Producto, ProductoAdmin)
admin.site.register(Pedido, PedidoAdmin)
admin.site.register(Most_Popular, Most_PopularAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Producto_home, Producto_homeAdmin)
