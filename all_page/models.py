from django.db import models
import os 
from django.conf import settings
from tinymce.models import HTMLField

class Category(models.Model):
    name = models.CharField(max_length=1500)
    def __unicode__(self):
        return self.name
    
class Producto(models.Model):
    photo = models.ImageField(upload_to='static/images/home', verbose_name='My Photo')
    name = models.CharField(max_length=200)
    description_short = models.CharField(max_length=1500, null=True, blank=True)
    description = HTMLField(null=True, blank=True)
    description_adicional = models.TextField(max_length=40000, null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    price_before = models.FloatField(null=True, blank=True)
    status = models.IntegerField(default=1)
    date = models.DateTimeField(auto_now_add=True)
    categoria = models.ForeignKey(Category, on_delete=models.CASCADE)
    
    photo1 = models.ImageField(upload_to='static/images/home', verbose_name='My Photo1', null=True, blank=True)
    photo2 = models.ImageField(upload_to='static/images/home', verbose_name='My Photo2', null=True, blank=True)
    photo3 = models.ImageField(upload_to='static/images/home', verbose_name='My Photo3', null=True, blank=True)
    photo4 = models.ImageField(upload_to='static/images/home', verbose_name='My Photo4', null=True, blank=True)
    
    def __unicode__(self):
        return self.name
	
    def filename(self):
        return os.path.basename(self.photo.name)
    def filename1(self):
        return os.path.basename(self.photo1.name)
    def filename2(self):
        return os.path.basename(self.photo2.name)
    def filename3(self):
        return os.path.basename(self.photo3.name)
    def filename4(self):
        return os.path.basename(self.photo4.name)
    
    def screenshots_as_list(self):
        return self.sizes.split(',')
    
    
    def admin_image(self):
        return '<img src="' + settings.STATIC_ADMIN +'/static/images/home/%s" style="width: 80px;"/>' % os.path.basename(self.photo.name)
    admin_image.allow_tags = True
    

    


class Pedido(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    price = models.FloatField(null=True, blank=True)
    email = models.CharField(max_length=1500)
    sent = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)
    

    
class Most_Popular(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    
    
class Producto_home(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    price = models.ImageField(upload_to='static/images/home', verbose_name='My Photo', null=True, blank=True)




