from django.shortcuts import render

# Create your views here.

# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import Producto, Most_Popular, Pedido, Category, Producto_home
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse,\
                        HttpResponseNotAllowed, HttpResponseBadRequest, \
                        HttpResponseForbidden
        

        
from django.core.mail import send_mail
from django.conf import settings

        
from django.core.mail import send_mail
from django.conf import settings

def index(request, category_id = None):
    template_name = 'index.html'
    data={}
    if category_id:
        data['actual_category']= Category.objects.get(id=category_id)
        data['productos']= Producto.objects.filter(categoria__id=category_id)
    else:
        data['actual_category']= Category.objects.get(id=1)
        data['productos']= Producto.objects.filter(categoria__id=1)
        
    
    data['category']= Category.objects.all()
    data['most_popular']= Most_Popular.objects.all()
    data['producto_home']= Producto_home.objects.all()
    return render_to_response(
		template_name, 
		data, 
		context_instance=RequestContext(request)
	)

def detalle(request, detalle_id):
    template_name = 'product-details.html'
    data={}
    data['category']= Category.objects.all()
    data['producto']= Producto.objects.get(id=detalle_id)
    data['most_popular']= Most_Popular.objects.all()
    data['producto_home']= Producto_home.objects.all()
    if request.method == 'POST':
        print request.POST
        quality = request.POST.get('quality',None)
        mail = request.POST.get('mail',None)
        producto = request.POST.get('producto',None)
        price = request.POST.get('price',None)
        producto= Producto.objects.get(id=producto)
        pedido= Pedido(producto=producto, amount=quality, price=price, email=mail)
        pedido.save()
        data['actual_category']= Category.objects.get(id=1)
        data['productos']= Producto.objects.filter(categoria__id=1)
        template_name = 'index.html'
        data['error'] = 'No poseemos stock del producto en este momento, te contactaremos en cuanto llegue el lote.'
        
    return render_to_response(
		template_name, 
		data, 
		context_instance=RequestContext(request)
	)

def cambio(request):
    template_name = 'cambio.html'
    data={}
    return render_to_response(
		template_name, 
		data, 
		context_instance=RequestContext(request)
	)
