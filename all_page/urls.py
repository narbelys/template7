from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^detalle/(?P<detalle_id>[0-9]+)$', views.detalle, name='detalle'),
    url(r'^home/(?P<category_id>[0-9]+)$', views.index, name='index'),
    url(r'^comprar/(?P<detalle_id>[0-9]+)$', views.detalle, name='detalle'),
    url(r'^cambio/', views.cambio, name='cambio'),
]